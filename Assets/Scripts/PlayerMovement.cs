using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    //Components
    private Rigidbody2D rb;
    private SpriteRenderer sprite;
    private new BoxCollider2D collider;

    //Objects
    [SerializeField] private new GameObject camera;
    [SerializeField] private GameObject archTarget;
    [SerializeField] private GameObject firePoint;

    //Layer Masks
    [SerializeField] private LayerMask jumpGround;

    //Movement parameters
    static public float moveX = 0f;
    [SerializeField] private float moveSpeed = 7f;
    [SerializeField] private float jumpForce = 14f;
    static public float facingRight = 1;

    //TargetObcject position relative to player
    [SerializeField] private float cameraObjectDistanceX = 2f;
    [SerializeField] private float cameraObjectDistanceY = 2f;

    //Arch position
    private float archObjectDistanceX = 1.8f;
    private float archObjectDistanceY = 1.25f;

    private float firePointDistanceX = 0.3f;
    private float firePointDistanceY = 1f;

    //Coyote Time
    [SerializeField] private float coyoteTime = 0.2f;
    private float coyoteTimeCounter;

    //Hiding


    // Start is called before the first frame update
    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        collider = GetComponent<BoxCollider2D>();
        sprite = GetComponent<SpriteRenderer>();
        camera.transform.position = new Vector2(rb.position.x + cameraObjectDistanceX, rb.position.y + cameraObjectDistanceY);
    }

    // Update is called once per frame
    private void Update()
    {
        //Movement
        moveX = Input.GetAxisRaw("Horizontal");

        if (IsGrounded())
        {
            coyoteTimeCounter = coyoteTime;
        }
        else
        {
            coyoteTimeCounter -= Time.deltaTime;
        }

        rb.velocity = new Vector2(moveX * moveSpeed, rb.velocity.y);

        if (Input.GetButtonDown("Jump") && coyoteTimeCounter > 0f)
        {
            rb.velocity = new Vector2(rb.velocity.x, jumpForce);
        }

        if (Input.GetButtonUp("Jump") && rb.velocity.y > 0f)
        {
            coyoteTimeCounter = 0f;
        }

        if (moveX < 0f)
        {
            facingRight = -1;
            sprite.flipX = true;
        }
        else if (moveX > 0f)
        {
            facingRight = 1;
            sprite.flipX = false;
        }

        UpdateCameraTarget();
        UpdateArch();
        UpdateFirePoint();
    }

    private void UpdateCameraTarget()
    {
        if (moveX > 0f)
        {
            camera.transform.position = new Vector2(rb.position.x + cameraObjectDistanceX, rb.position.y + cameraObjectDistanceY);
        }
        else if (moveX < 0f)
        {
            camera.transform.position = new Vector2(rb.position.x - cameraObjectDistanceX, rb.position.y + cameraObjectDistanceY);
        }
    }
    private void UpdateArch()
    {
        if (moveX > 0f)
        {
            archTarget.transform.position = new Vector2(rb.position.x + archObjectDistanceX, rb.position.y + archObjectDistanceY);
        }
        else if (moveX < 0f)
        {
            archTarget.transform.position = new Vector2(rb.position.x - archObjectDistanceX, rb.position.y + archObjectDistanceY);
        }
    }
    private void UpdateFirePoint()
    {
        if (moveX > 0f)
        {
            firePoint.transform.position = new Vector2(rb.position.x + firePointDistanceX, rb.position.y + firePointDistanceY);
        }
        else if (moveX < 0f)
        {
            firePoint.transform.position = new Vector2(rb.position.x - firePointDistanceX, rb.position.y + firePointDistanceY);
        }
    }

    private bool IsGrounded()
    {
        return Physics2D.BoxCast(collider.bounds.center, collider.bounds.size, 0f, Vector2.down, .4f, jumpGround);
    }
}
