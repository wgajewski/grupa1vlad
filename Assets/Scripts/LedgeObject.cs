using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LedgeObject : MonoBehaviour
{
    [SerializeField] Sprite s1, s2;
    [SerializeField] private GameObject stump;
    private SpriteRenderer spriteRenderer;

    // Start is called before the first frame update
    void Start()
    {
        spriteRenderer = stump.GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("axe"))
        {
            spriteRenderer.sprite = s2;
        }
    }
}
