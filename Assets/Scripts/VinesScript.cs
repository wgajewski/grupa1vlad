using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VinesScript : MonoBehaviour
{
    private BoxCollider2D _boxCollider;
    private SpriteRenderer _sprite;
    [SerializeField] private CloudAndMoonScript moon;
    private void Awake()
    {
        _sprite = GetComponent<SpriteRenderer>();
        _boxCollider = GetComponent<BoxCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if(moon.isClouded == true)
        {
            _sprite.enabled = false;
            _boxCollider.enabled = false;
        }
        else if(moon.isClouded == false)
        {
            _sprite.enabled = true;
            _boxCollider.enabled = true;
        }
    }
}
