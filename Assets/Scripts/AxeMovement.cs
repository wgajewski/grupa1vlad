using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AxeMovement : MonoBehaviour
{
    private Rigidbody2D rb;
    [SerializeField] private float speed = 2f;
    [SerializeField] private float speedUp = 2f;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.velocity = new Vector2(PlayerMovement.facingRight * speed, speedUp);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("ledge"))
        {
            Object.Destroy(this.gameObject);
        }

        if (other.gameObject.CompareTag("ground"))
        {
            Object.Destroy(this.gameObject);
        }
    }
}
